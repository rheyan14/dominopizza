angular.module('DominoPizza', [
    'ui.router',
    'DominoPizza.List'
]).config(['$urlRouterProvider', '$stateProvider', '$httpProvider',
    function($urlRouterProvider, $stateProvider, $httpProvider) {
        $stateProvider
            .state('pizzas', {
                url: '/pizzas',
                templateUrl: 'app/components/PizzaOrders/partials/pizzaOrder_main.html',
                abstract: true
            });
        $urlRouterProvider.otherwise('/not_found');
    }
]);